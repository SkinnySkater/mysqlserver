#docker exec -it mariadb bash
#mysql -u root -ppassword1
#CREATE DATABASE new_database;
#mariadb mysql -uroot -ppassword1 --database=new_database < dump.sql
#docker exec -i mariadb mysql -uroot -ppassword1 --database=new_database < dump.sql  


-- Structure de la table `recipes__category`
DROP DATABASE IF EXISTS employees;
CREATE DATABASE IF NOT EXISTS employees;
USE employees;

CREATE TABLE employees (
    emp_no      INT             NOT NULL,
    birth_date  DATE            NOT NULL,
    first_name  VARCHAR(14)     NOT NULL,
    last_name   VARCHAR(16)     NOT NULL,
    gender      ENUM ('M','F')  NOT NULL,    
    hire_date   DATE            NOT NULL,
    PRIMARY KEY (emp_no)
);

CREATE TABLE departments (
    dept_no     CHAR(4)         NOT NULL,
    dept_name   VARCHAR(40)     NOT NULL,
    PRIMARY KEY (dept_no),
    UNIQUE  KEY (dept_name)
);

--step 1
CREATE TABLE salaries_grid ( 
 grid_no 				int NOT NULL AUTO_INCREMENT, 
 seniority_year_req	 	int NOT NULL, 
 min_salary				FLOAT NOT NULL, 
 max_salary				FLOAT NOT NULL, 
 dept_no 				char(4) NOT NULL, 
 PRIMARY KEY (grid_no) 
  ) ENGINE = InnoDB DEFAULT CHARACTER SET = latin1;

#CREATE TABLE salaries_grid ( grid_no int NOT NULL AUTO_INCREMENT, seniority_year_req int NOT NULL, min_salary FLOAT NOT NULL, max_salary FLOAT NOT NULL, dept_no char(4) NOT NULL, PRIMARY KEY (grid_no) ) ENGINE = InnoDB DEFAULT CHARACTER SET = latin1;
--step 2
ALTER TABLE employees ADD grid_no int AFTER hire_date;
ALTER TABLE employees ADD FOREIGN KEY (grid_no) REFERENCES salaries_grid (grid_no);
CREATE UNIQUE INDEX fk_employees_salaries_grid_idx ON employees (grid_no);

--step3
ALTER TABLE salaries_grid ADD FOREIGN KEY (dept_no) REFERENCES departments (dept_no) ON DELETE CASCADE;

--step 4 :Écrire la requête qui permet de charger la base CSV des grilles de salaire dans la table employees.salaries_grid.
